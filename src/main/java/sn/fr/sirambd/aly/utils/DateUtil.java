package sn.fr.sirambd.aly.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    /**
     * Default formatDate
     * format the date to french date with time
     *
     * @param date the date to be formatted
     * @return the string of the formatted date
     */
    public static String formatDate(Date date) {
        return formatDate(date, "dd-MM-yyyy à HH:mm");
    }

    /**
     * Return a formatted date by passing the format pattern
     *
     * @param date          the date to be formatted
     * @param formatPattern the pattern to use to format
     * @return the string of the formatted date
     */
    public static String formatDate(Date date, String formatPattern) {
        SimpleDateFormat sf = new SimpleDateFormat(formatPattern, Locale.FRANCE);
        return sf.format(date);
    }
}
