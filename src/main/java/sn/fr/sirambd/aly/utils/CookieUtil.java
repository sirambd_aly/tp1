package sn.fr.sirambd.aly.utils;

import sn.fr.sirambd.aly.servlets.ClientServlet;

import javax.servlet.http.Cookie;

public class CookieUtil {

    public static Cookie findByName(String name, Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(name)) return cookie;
            }
        }
        return null;
    }

    public static Boolean isAuth(Cookie[] cookies) {
        Cookie cookie = findByName(ClientServlet.CLIENT_COOKIE, cookies);
        return cookie != null;
    }
}
