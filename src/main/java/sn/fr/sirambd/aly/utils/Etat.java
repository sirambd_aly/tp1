package sn.fr.sirambd.aly.utils;

public class Etat {
    public static final String ENREGISTREMENT = "Enregistrement";
    public static final String ATTENTE = "En attente";
    public static final String ACHEMINEMENT = "En acheminement";
    public static final String BLOQUE = "Bloqué";
    public static final String LIVRE = "Livré";
}
