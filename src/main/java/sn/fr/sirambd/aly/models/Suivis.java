package sn.fr.sirambd.aly.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "suivis")
public class Suivis implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "suivis_id", nullable = false)
    private int id;

    @Column(name = "suivis_lat", nullable = false)
    private double latitude;

    @Column(name = "suivis_long", nullable = false)
    private double longitude;

    @Column(name = "suivis_emplacement", nullable = false)
    private String emplacement;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "colis_id", referencedColumnName = "colis_id", nullable = false)
    private Colis colis;

    public Suivis() {
    }

    public Suivis(double latitude, double longitude, String emplacement) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.emplacement = emplacement;
    }

    public int getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getEmplacement() {
        return emplacement;
    }

    public void setEmplacement(String emplacement) {
        this.emplacement = emplacement;
    }

    public Colis getColis() {
        return colis;
    }

    public void setColis(Colis colis) {
        this.colis = colis;
    }
}
