package sn.fr.sirambd.aly.models;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "clients")
public class Client implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "client_id", unique = true)
    private int id;

    @Column(name = "client_name", nullable = false)
    private String name;

    @Column(name = "client_age", nullable = false)
    private int age;

    public Client() {
        setName("");
        setAge(0);
    }

    public Client(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Informations Client" + id + "\t" + name + "\t" + age;
    }
}
