package sn.fr.sirambd.aly.models;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "etats")
public class Etat implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "etat_id", unique = true)
    private int id;

    @Column(name = "etat_name", unique = true)
    private String name;

    public Etat() {
    }

    public Etat(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Informations Colis:" + id + "\t" + name;
    }
}
