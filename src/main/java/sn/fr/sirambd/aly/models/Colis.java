package sn.fr.sirambd.aly.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table(name = "colis")
public class Colis implements Serializable {

    private static final long serialVersionUID = -7877074662902674526L;

    @Id
    @GeneratedValue
    @Column(name = "colis_id", updatable = false)
    private int id;

    @Column(name = "colis_poids", nullable = false)
    private Double poids;

    @Column(name = "colis_valeur", nullable = false)
    private Double valeur;

    @Column(name = "colis_origine", nullable = false)
    private String origine;

    @Column(name = "colis_destination", nullable = false)
    private String destination;

    @OneToOne
    @JoinColumn(name = "etat_id", referencedColumnName = "etat_id", columnDefinition = "varchar(32) default 1")
    private Etat etat;

    @OneToMany(mappedBy = "colis")
    @OrderBy(value = "id DESC")
    private List<Suivis> suivis = new LinkedList<>();

    public Colis() {
    }

    public Colis(Double poids, Double valeur, String origine, String destination) {
        this.poids = poids;
        this.valeur = valeur;
        this.origine = origine;
        this.destination = destination;
    }

    public Colis(Double poids, Double valeur, String origine, String destination, Etat etat) {
        this.poids = poids;
        this.valeur = valeur;
        this.origine = origine;
        this.destination = destination;
        this.etat = etat;
    }

    public void addSuivis(Suivis suivis) {
        suivis.setColis(this);
        this.suivis.add(suivis);
    }

    public int getId() {
        return id;
    }

    public Double getPoids() {
        return poids;
    }

    public void setPoids(Double poids) {
        this.poids = poids;
    }

    public Double getValeur() {
        return valeur;
    }

    public void setValeur(Double valeur) {
        this.valeur = valeur;
    }

    public String getOrigine() {
        return origine;
    }

    public void setOrigine(String origine) {
        this.origine = origine;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<Suivis> getSuivis() {
        suivis.sort((o1, o2) -> o2.getId() - o1.getId());
        return suivis;
    }

    public void setSuivis(List<Suivis> suivis) {
        this.suivis = suivis;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    @Override
    public String toString() {
        return "Informations Colis:" + id + "\t" + poids + "\t" + valeur + "\t" + origine + "\t" + destination;
    }
}

