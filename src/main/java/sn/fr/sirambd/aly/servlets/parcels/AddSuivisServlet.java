package sn.fr.sirambd.aly.servlets.parcels;

import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.ejb.SuivisEJB;
import sn.fr.sirambd.aly.models.Colis;
import sn.fr.sirambd.aly.models.Suivis;
import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddSuivisServlet", value = "/suivis/create")
public class AddSuivisServlet extends HttpServlet {
    private static final long serialVersionUID = -8117387100807246615L;

    @EJB
    private SuivisEJB suivisEJB;
    @EJB
    private ColisEJB colisEJB;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (CookieUtil.isAuth(req.getCookies())) {
            int parcelID = Integer.parseInt(req.getParameter("parcel_id"));
            double latitude = Double.parseDouble(req.getParameter("latitude"));
            double longitude = Double.parseDouble(req.getParameter("longitude"));
            String emplacement = req.getParameter("emplacement");

            suivisEJB.create(latitude, longitude, emplacement, parcelID);

            req.setAttribute("Success!", "Suivis ajouté avec succès.");
            resp.sendRedirect("../client");
        } else {
            resp.sendRedirect("../client");
        }
    }
}
