package sn.fr.sirambd.aly.servlets;

import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static sn.fr.sirambd.aly.servlets.ClientServlet.CLIENT_COOKIE;

@WebServlet(name = "Logout", value = "/logout")
public class LogoutServlet extends HttpServlet {

    private static final long serialVersionUID = -3143111378491553010L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = CookieUtil.findByName(CLIENT_COOKIE, req.getCookies());
        if (cookie != null) {
            Cookie rsCookie = new Cookie(CLIENT_COOKIE, "");
            rsCookie.setMaxAge(0);
            resp.addCookie(rsCookie);
        }
        resp.sendRedirect("client");
    }
}
