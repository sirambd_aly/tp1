package sn.fr.sirambd.aly.servlets;

import sn.fr.sirambd.aly.ejb.ClientEJB;
import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.ejb.EtatEJB;
import sn.fr.sirambd.aly.ejb.SuivisEJB;

import javax.ejb.EJB;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyContextListener implements ServletContextListener {
    @EJB
    private ColisEJB colisEJB;
    @EJB
    private EtatEJB etatEJB;
    @EJB
    private ClientEJB clientEJB;
    @EJB
    private SuivisEJB suivisEJB;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        colisEJB.getEm().flush();
        colisEJB.getEm().close();

        etatEJB.getEm().flush();
        etatEJB.getEm().close();

        clientEJB.getEm().flush();
        clientEJB.getEm().close();

        suivisEJB.getEm().flush();
        suivisEJB.getEm().close();
    }
}
