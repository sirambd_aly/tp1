package sn.fr.sirambd.aly.servlets.parcels;

import sn.fr.sirambd.aly.ejb.ClientEJB;
import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.ejb.EtatEJB;
import sn.fr.sirambd.aly.models.Client;
import sn.fr.sirambd.aly.models.Colis;
import sn.fr.sirambd.aly.servlets.ClientServlet;
import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Colis", value = "/colis")
public class ColisServlet extends HttpServlet {
    private static final long serialVersionUID = 7261506987508619207L;

    @EJB
    private ColisEJB colisEJB;
    @EJB
    private ClientEJB clientEJB;
    @EJB
    private EtatEJB etatEJB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Colis> parcels = colisEJB.getAll();
        Cookie cookie = CookieUtil.findByName(ClientServlet.CLIENT_COOKIE, req.getCookies());
        if (cookie == null) {
            resp.sendRedirect("client");
        } else {
            Client auth = clientEJB.find(Integer.parseInt(cookie.getValue()));

            req.setAttribute("auth", auth);
            req.setAttribute("parcels", parcels);
            req.setAttribute("statuses", etatEJB.getAll());
            req.setAttribute("canEdit", auth.getId() != 3);
            req.setAttribute("canCreate", auth.getId() == 1);

            getServletContext().getRequestDispatcher("/welcome.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
