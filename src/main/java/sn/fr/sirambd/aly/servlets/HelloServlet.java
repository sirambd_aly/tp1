package sn.fr.sirambd.aly.servlets;

import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.ejb.EtatEJB;
import sn.fr.sirambd.aly.ejb.SuivisEJB;
import sn.fr.sirambd.aly.models.Colis;
import sn.fr.sirambd.aly.models.Suivis;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;

@WebServlet(name = "Home", value = "/hello")
public class HelloServlet extends HttpServlet {

    @EJB
    ColisEJB ejb;
    @EJB
    private SuivisEJB suivisEJB;
    @EJB
    private EtatEJB etatEJB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        suivisEJB.find(1);
        Colis colis = ejb.create(12d, 20d, "Mayotte", "Sénégal", etatEJB.find(1));
        resp.getWriter().println(String.format("Le colis %d a bien été créé", colis.getId()));
    }
}
