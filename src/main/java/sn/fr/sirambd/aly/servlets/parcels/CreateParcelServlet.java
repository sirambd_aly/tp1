package sn.fr.sirambd.aly.servlets.parcels;

import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.ejb.EtatEJB;
import sn.fr.sirambd.aly.models.Etat;
import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "CreateParcel", value = "/parcel/create")
public class CreateParcelServlet extends HttpServlet {
    private static final long serialVersionUID = -6460619514476587868L;

    @EJB
    private ColisEJB colisEJB;
    @EJB
    private EtatEJB etatEJB;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (CookieUtil.isAuth(req.getCookies())) {
            double
                    poids = Double.parseDouble(req.getParameter("poids")),
                    valeur = Double.parseDouble(req.getParameter("valeur"));
            String
                    origine = req.getParameter("origine"),
                    destination = req.getParameter("destination");

            Etat etat = etatEJB.find(1);
            colisEJB.create(poids, valeur, origine, destination, etat);
            resp.sendRedirect("../client");
        } else {
            resp.sendRedirect("../client");
        }
    }
}
