package sn.fr.sirambd.aly.servlets.parcels;


import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteParcel", value = "/parcel/delete")
public class DeleteParcel extends HttpServlet {
    private static final long serialVersionUID = 7916013804628520765L;

    @EJB
    private ColisEJB colisEJB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (CookieUtil.isAuth(req.getCookies())) {
            int id = Integer.parseInt(req.getParameter("id"));
            colisEJB.destroy(id);
            resp.sendRedirect("../client");
        } else {
            resp.sendRedirect("../client");
        }
    }
}
