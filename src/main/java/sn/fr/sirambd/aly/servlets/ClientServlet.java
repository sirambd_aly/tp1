package sn.fr.sirambd.aly.servlets;

import sn.fr.sirambd.aly.ejb.ClientEJB;
import sn.fr.sirambd.aly.models.Client;
import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "Client", value = "/client")
public class ClientServlet extends HttpServlet {

    public static final String CLIENT_COOKIE = "client";
    @EJB
    private ClientEJB clientEJB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = CookieUtil.findByName(CLIENT_COOKIE, req.getCookies());
        if (cookie == null) {
            ArrayList<Client> clients = new ArrayList<>(clientEJB.getAll());
            req.setAttribute("clients", clients);
            req.setAttribute("title", "TPPM - Connexion");
            this.getServletContext().getRequestDispatcher("/welcome.jsp").forward(req, resp);
        } else {
            resp.sendRedirect("colis");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie cookie = CookieUtil.findByName(CLIENT_COOKIE, req.getCookies());
        if (cookie == null || cookie.getValue().isEmpty()) {
            resp.addCookie(new Cookie(CLIENT_COOKIE, req.getParameter("client_id")));
        }
        resp.sendRedirect("colis");
    }
}
