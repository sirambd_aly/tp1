package sn.fr.sirambd.aly.servlets.parcels;

import sn.fr.sirambd.aly.ejb.ColisEJB;
import sn.fr.sirambd.aly.utils.CookieUtil;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UpdateStatus", value = "/parcel/status/update")
public class UpdateStatusServlet extends HttpServlet {
    private static final long serialVersionUID = 7638113582451440418L;

    @EJB
    private ColisEJB colisEJB;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (CookieUtil.isAuth(req.getCookies())) {
            int parcelID = Integer.parseInt(req.getParameter("parcel_id"));
            int statusID = Integer.parseInt(req.getParameter("stat_id"));

            colisEJB.updateState(parcelID, statusID);
            resp.sendRedirect("../../client");
        } else {
            resp.sendRedirect("../../client");
        }
    }
}
