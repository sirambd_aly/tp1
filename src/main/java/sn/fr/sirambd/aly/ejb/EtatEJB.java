package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.models.Etat;
import sn.fr.sirambd.aly.utils.Constant;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class EtatEJB {
    @PersistenceContext
    private EntityManager em;

    public EtatEJB() {
    }

    public Etat create(String name) { // Les valeur du colis à passer en paramètre
        Etat client = new Etat(name);
        em.persist(client);
        return client;
    }

    public Etat update(Etat etat) {
        return em.merge(etat);
    }

    public Etat find(int id) {
        Etat etat = em.find(Etat.class, id);
        if (etat == null)
            throw new EntityNotFoundException(String.format(Constant.ENTITY_NOT_FOUND_EXCEPTION, "Etat", id));
        return etat;
    }

    public Etat find(String name) {
        Query query = em.createQuery("select e from Etat e where e.name = :name", Etat.class);
        query.setParameter("name", name);
        return (Etat) query.getSingleResult();

    }

    public List<Etat> getAll() {
        Query query = em.createQuery("select a from Etat a", Etat.class);
        return query.getResultList();
    }

    public EntityManager getEm() {
        return em;
    }
}
