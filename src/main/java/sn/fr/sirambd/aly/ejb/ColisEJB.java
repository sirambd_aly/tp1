package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.models.Colis;
import sn.fr.sirambd.aly.models.Etat;
import sn.fr.sirambd.aly.utils.Constant;

import javax.annotation.sql.DataSourceDefinition;
import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@DataSourceDefinition(
        name = "java:app/env/jdbc/MyDataSource",
        className = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource",
        user = "root",
        password = "",
        serverName = "127.0.0.1",
        portNumber = 3306,
        databaseName = "tppm_db",
        properties = {"useUnicode=true", "useFastDateParsing=false", "characterEncoding=UTF-8", "zeroDateTimeBehavior=convertToNull"})


@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class ColisEJB {

    @PersistenceContext
    private EntityManager em;

    public ColisEJB() {
    }

    public Colis create(Double poids, Double valeur, String origine, String destination, Etat etat) { // Les valeur du colis à passer en paramètre
        try {
            Colis colis = new Colis(poids, valeur, origine, destination, etat);
            em.persist(colis);
            return colis;
        } catch (Exception e) {
            return null;
        }
    }

    public Colis update(Colis colis) {
        return em.merge(colis);
    }

    public Colis updateState(int parcelID, int stateID) {
        Colis parcel = em.find(Colis.class, parcelID);
        Etat state = em.find(Etat.class, stateID);
        parcel.setEtat(state);
        return parcel;
    }

    public Colis find(int id) {
        Colis colis = em.find(Colis.class, id);
        if (colis == null)
            throw new EntityNotFoundException(String.format(Constant.ENTITY_NOT_FOUND_EXCEPTION, "Colis", id));
        return colis;
    }

    public void destroy(int id) {
        Colis colis = em.find(Colis.class, id);
        em.remove(colis);
    }

    public List<Colis> getAll() {
        Query query = em.createQuery("select a from Colis a order by a.id desc", Colis.class);
        List<Colis> res = query.getResultList();
        return res;
    }

    public EntityManager getEm() {
        return em;
    }
}