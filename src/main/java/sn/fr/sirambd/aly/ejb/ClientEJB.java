package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.models.Client;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)

public class ClientEJB {

    @PersistenceContext
    private EntityManager em;

    public ClientEJB() {
    }

    public Client create(String name, int age) { // Les valeur du colis à passer en paramètre
        Client client = new Client(name, age);
        em.persist(client);
        return client;
    }

    public Client update(Client client) {
        return em.merge(client);
    }

    public Client find(int idClient) {
        Client client = em.find(Client.class, idClient);
        if (client == null) {
            throw new EntityNotFoundException(" Client introuvable "
                    + idClient);
        }
        return client;
    }

    public List<Client> getAll() {
        Query query = em.createQuery("select a from Client a", Client.class);
        return query.getResultList();
    }

    public EntityManager getEm() {
        return em;
    }
}
