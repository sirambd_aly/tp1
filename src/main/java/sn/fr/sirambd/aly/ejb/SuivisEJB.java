package sn.fr.sirambd.aly.ejb;

import sn.fr.sirambd.aly.models.Colis;
import sn.fr.sirambd.aly.models.Suivis;
import sn.fr.sirambd.aly.utils.Constant;

import javax.ejb.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@LocalBean // Utiliser dans le client sans interface
@Stateless
@Remote
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class SuivisEJB {

    @PersistenceContext
    private EntityManager em;

    public SuivisEJB() {
    }

    public Suivis find(int id) {
        Suivis suivis = em.find(Suivis.class, id);
        if (suivis == null)
            throw new EntityNotFoundException(String.format(Constant.ENTITY_NOT_FOUND_EXCEPTION, "Suivis", id));
        return suivis;
    }

    public Suivis create(double latitude, double longitude, String emplacement, int colisID) {
        Suivis suivis = new Suivis(latitude, longitude, emplacement);
        Colis c = em.find(Colis.class, colisID);
        c.addSuivis(suivis);
        em.persist(suivis);
        return suivis;
    }

    public EntityManager getEm() {
        return em;
    }
}
