<%--
  Created by IntelliJ IDEA.
  User: AbdourahamaneBOINAID
  Date: 03/11/2019
  Time: 21:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<%@include file="include_html_head.jsp" %>
<body>
<%@ include file="include_body_nav.jsp" %>

<%@ include file="include_body_header.jsp" %>

<c:if test="${auth != null}">
    <c:set var="index_modal" value="0"/>
    <section class="py-5 text-center">
        <article class="container">
            <div class="row">
                <div class="bg-white p-5 mx-auto col-md-8 col-10">
                    <h3 class="display-3">Liste des colis</h3>

                    <!-- Ajouter un colis -->
                    <c:if test="${canCreate}">
                        <div class="text-right mb-3">
                            <button data-toggle="modal" class="btn btn-primary text-right"
                                    data-target="#modal_create_parcel">
                                Ajouter un colis
                            </button>
                            <%@include file="modal_create_parcel.jsp" %>
                        </div>
                    </c:if>

                    <!-- Afficher les colis -->
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Libellé</th>
                            <th>Status</th>
                            <th>Localisation</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="parcel" items="${parcels}">
                            <tr>
                                <td>${parcel.id}</td>
                                <td>De ${parcel.origine} à ${parcel.destination}</td>
                                <td>${parcel.etat.name}</td>
                                <c:if test="${empty parcel.suivis}">
                                    <td>Pas disponible</td>
                                </c:if>
                                <c:if test="${!empty parcel.suivis}">
                                    <td>${parcel.suivis.get(0).emplacement}</td>
                                </c:if>
                                <td>
                                    <div class="dropdown">
                                        <a href="#" role="button" id="dropdownMenuLink${parcel.id}"
                                           data-toggle="dropdown"
                                           aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink${parcel.id}"
                                             style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 18px, 0px);"
                                             x-placement="bottom-start">
                                            <a class="dropdown-item" href="#" data-toggle="modal"
                                               data-target="#modal_edit_parcel_${parcel.id}">
                                                Voir
                                            </a>
                                            <c:if test="${canCreate}">
                                                <a class="dropdown-item" href="<c:url value="/parcel/delete?id=${parcel.id}"/>">Supprimer</a>
                                            </c:if>
                                        </div>
                                    </div>
                                </td>
                                <%@include file="modal_edit_parcel.jsp" %>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </article>
    </section>
</c:if>

<%@ include file="include_content_context_authors.jsp" %>

<%@ include file="include_body_footer.jsp" %>
</body>
</html>
