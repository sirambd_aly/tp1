<%--
  Created by IntelliJ IDEA.
  User: AbdourahamaneBOINAID
  Date: 06/11/2019
  Time: 21:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="modal fade" id="modal_create_parcel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <h4 class="modal-title text-dark mx-auto">
                    <b>Créer un colis</b>
                </h4>
                <button type="button" class="close text-danger pt-4 ml-0" data-dismiss="modal">
            <span>
              <i class="now-ui-icons ui-1_simple-remove lg"></i>
            </span>
                </button>
            </div>
            <div class="modal-body">
                <form id="form_create_parcel" class="text-left" method="post" action="<c:url value="/parcel/create"/>">
                    <div class="form-group">
                        <label for="poids">Poids</label>
                        <input type="number" id="poids" class="form-control" name="poids" placeholder="poids"/>
                    </div>
                    <div class="form-group">
                        <label for="valeur">Valeur</label>
                        <input type="number" id="valeur" class="form-control" name="valeur" placeholder="valeur"/>
                    </div>
                    <div class="form-group">
                        <label for="origine">Origine</label>
                        <input type="text" id="origine" class="form-control" name="origine" placeholder="origine"/>
                    </div>
                    <div class="form-group">
                        <label for="destination">Destination</label>
                        <input type="text" id="destination" class="form-control" name="destination"
                               placeholder="destination"/>
                    </div>
                </form>
            </div>

            <div class="modal-footer d-flex flex-row justify-content-between">
                <button type="submit" class="btn btn-secondary" form="form_create_parcel">Créer</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>