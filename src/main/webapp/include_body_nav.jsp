<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<nav class="navbar navbar-expand-lg bg-primary navbar-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" title="TPPM - Gestion de colis. Créer par Abdourahamane BOINAIDI et Aly-Nayef BASMA"
           data-placement="bottom"
           data-toggle="tooltip" href="#">GESTION DE COLIS</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarNowUIKitFree">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse text-center justify-content-end" id="navbarNowUIKitFree">
            <ul class="navbar-nav">
                <c:if test="${auth != null}">
                    <li class="nav-item mx-1 align-items-center d-flex justify-content-center">
                        <a class="nav-link">${auth.name}</a>
                    </li>
                </c:if>
                <li class="nav-item mx-1 align-items-center d-flex justify-content-center">
                    <a class="nav-link" href="<c:url value='/#context'/>">
                        <i class="now-ui-icons travel_info x2 mr-2"></i>&nbsp;OBJECTIFS</a>
                </li>
                <li class="nav-item mx-1 align-items-center d-flex justify-content-center">
                    <a class="nav-link" href="<c:url value='/#teams'/>">
                        <i class="now-ui-icons users_circle-08 x2 mr-2"></i>NOTRE EQUIPES</a>
                </li>
            </ul>
            <c:if test="${auth == null}">
                <a class="btn btn-light text-primary" href="#pro">
                    <i class="fa fa-sign-in mr-1"> Se&nbsp; connecter</i>
                </a>
            </c:if>
            <c:if test="${auth != null}">
                <a class="btn btn-light text-primary" href="<c:url value='/logout'/>">
                    <i class="fa fa-sign-out mr-1"> Se déconnecter</i>
                </a>
            </c:if>
            <ul class="navbar-nav flex-row justify-content-center mt-2 mt-md-0">
                <li class="nav-item mx-1">
                    <a class="nav-link" href="#" data-placement="bottom" data-toggle="tooltip"
                       title="Follow us on Twitter">
                        <i class="fa fa-fw fa-twitter fa-2x"></i>
                    </a>
                </li>
                <li class="nav-item mx-3 mx-md-1">
                    <a class="nav-link" href="#" data-placement="bottom" data-toggle="tooltip"
                       title="Like us on Facebook">
                        <i class="fa fa-fw fa-facebook-official fa-2x"></i>
                    </a>
                </li>
                <li class="nav-item ml-1">
                    <a class="nav-link" href="#" data-placement="bottom" data-toggle="tooltip"
                       title="Follow us on Instagram">
                        <i class="fa fa-fw fa-instagram fa-2x"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>