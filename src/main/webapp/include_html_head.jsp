<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          type="text/css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.standalone.min.css">
    <link rel="stylesheet" href="<c:url value='/css/style.css'/>" type="text/css">
    <link rel="stylesheet" href="<c:url value='/css/nucleo-icons.css'/>" type="text/css">
    <script src="<c:url value='/js/navbar-ontop.js'/>"></script>
    <c:if test="${title == null }">
        <c:set var="title_name" value="TPPM - BIENVENU"/>
    </c:if>
    <c:if test="${title != null }">
        <c:set var="title_name" value="${title}"/>
    </c:if>
    <title>${title_name}</title>
    <meta name="description" content="Séléctionner votre client pour accéder à ces ressources">
    <meta name="keywords"
          content="bootstrap 4, bootstrap 4 uit kit, bootstrap 4 kit, now ui, now ui kit, creative tim, html kit, html css template, web template, bootstrap, bootstrap 4, css3 template, frontend, responsive bootstrap template, bootstrap ui kit, responsive ui kit, sirambd, tppm, gestion colis">
</head>