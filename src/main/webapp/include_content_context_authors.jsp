<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<section class="py-3 text-center" id="context">
    <article class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Les rôles des différents clients</h1>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-4 p-4"><i class="d-block fa fa-circle fa-3x mb-2 text-muted"></i>
                <h4><b>One</b></h4>
                <p>Le client 1,&nbsp; crée les colis</p>
            </div>
            <div class="col-md-4 col-6 p-4"><i class="d-block fa fa-stop-circle-o fa-3x mb-2 text-muted"></i>
                <h4><b>Two</b></h4>
                <p>Le deuxième, pourra simplement changer le status des colis.</p>
            </div>
            <div class="col-md-4 col-6 p-4"><i class="d-block fa fa-circle-o fa-3x mb-2 text-muted"></i>
                <h4><b>Three</b></h4>
                <p>Celui-ci pourra simplement suivre les colis.</p>
            </div>
        </div>
    </article>
</section>
<section class="py-5 text-center text-white" id="teams"
         style="background-image: linear-gradient(to bottom, rgba(0, 0, 0, .75), rgba(0, 0, 0, .75)),
                 url(<c:url value='/img/usmb.jpg'/>);
                 background-position: center center, center center; background-size: cover, cover; background-repeat: repeat, repeat;">
    <article class="container">
        <div class="row">
            <div class="mx-auto col-md-12">
                <h1 class="mb-3">Notre équipe</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 p-4"><img class="img-fluid d-block mb-3 mx-auto rounded-circle"
                                                    src="<c:url value='/img/man-boss.png' />"
                                                    alt="Card image cap" width="200">
                <h4><b>B. Abdourahamane</b></h4>
                <p class="mb-0">DEV. FULLSTACK &amp; DESIGNER</p>
            </div>
            <div class="col-lg-6 col-md-6 p-4"><img class="img-fluid d-block mb-3 mx-auto rounded-circle"
                                                    src="<c:url value='/img/man-boss.png' />"
                                                    alt="Card image cap" width="200">
                <h4><b>B. Aly-Nayef</b></h4>
                <p class="mb-0">DEV. FULLSTACK</p>
            </div>
        </div>
    </article>
</section>