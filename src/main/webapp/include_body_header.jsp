<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<header class="py-5 text-center parallax cover gradient-overlay h-25"
        style=" background-image: url(<c:url value='/img/header.jpg'/>);
                position: relative; background-position: center center; background-size: cover; background-repeat: repeat;
                ">
    <div class="container d-flex flex-column">
        <div class="row my-auto">
            <div class="mx-auto col-md-6 col-10 bg-white p-5">
                <h1 class="mb-4">Compte</h1>
                <form method="post" action="<c:url value="/client" />">
                    <input type="hidden" name="client" value="1" style="">
                    <!-- client 1-->
                    <div class="form-group mb-3">
                        <c:if test="${auth == null}">
                        <select class="form-control" name="client_id">
                            </c:if>
                            <c:if test="${auth != null}">
                            <select class="form-control" name="client_id" disabled>
                                </c:if>
                                <option disabled="">Choisissez un client</option>
                                <c:forEach var="client" items="${clients}">
                                    <option value="${client.id}">client ${client.id} - ${client.name}</option>
                                </c:forEach>
                            </select>
                            <small class="form-text text-muted text-right">
                                <a href="#">Choisissez un client</a>
                            </small>
                    </div>
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-md-12">
                <h6 class="text-muted mb-5">
                    <b class="text-white">Suivis de colis. TP2 - Java J2EE. Avec Stephane Talbot, USMB</b></h6>
            </div>
        </div>
    </div>
</header>

<c:if test="${!empty error}">
    <div class="alert alert-danger alert-dismissible fade show" role="alert"
         style="position: fixed;/* text-align: end; *//* float: inherit; */right: 0;top: 100px;z-index: 1;/* color: white; */">
        <strong>Une erreur a été rencontrer!</strong> ${error}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</c:if>
<c:if test="${!empty success}">
    <div class="alert alert-success alert-dismissible fade show" role="alert"
         style="position: fixed;/* text-align: end; *//* float: inherit; */right: 0;top: 100px;z-index: 1;/* color: white; */">
        <strong>Success!</strong> ${success}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</c:if>