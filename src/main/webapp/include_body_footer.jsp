<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer class="py-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-6 p-3"><i class="d-block fa fa-circle-o fa-5x text-primary"></i></div>
            <div class="col-lg-4 col-6 p-3">
                <p><a href="https://www.univ-smb.fr/" target="_blank">Université Savoie Mont Blanc&nbsp;<br>74000
                    Bourget du lac<br>FRANCE</a></p>
                <p><a href="tel:+33 04 79 75 85 85">univ-smb.fr</a></p>
                <p class="mb-0"><a href="mailto:b.abdourahamane@outlook.fr">b.abdourahamane@outlook.fr</a></p>
            </div>
            <div class="col-md-4 p-3">
                <h5><b>A propos</b></h5>
                <p class="mb-0"> Ceci est un tp réalisé en cours lors d'un travaux pratique.&nbsp;<br>Le but ici est de
                    réaliser un gestion de colis, en utilisant la technologie J2EE. On a ici 3 roles, le client 1 qui
                    crée les colis, le client 2 qui change l'état et les coordonnées du colis, et enfin un client 3 qui
                    peut juste consulter les colis.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p class="mb-0 mt-2">© 2019-2020 B.Abdourahamane B.Aly-Nayef. Tout droit réservé</p>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"
        style=""></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"
        style=""></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"
        style=""></script>
<script>
    $(document).ready(function () {
        $(".alert").delay(4000).slideUp(200, function () {
            $(this).alert('close');
        });
    });
</script>