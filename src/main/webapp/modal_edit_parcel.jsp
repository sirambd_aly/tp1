<%--
  Created by IntelliJ IDEA.
  User: AbdourahamaneBOINAID
  Date: 06/11/2019
  Time: 21:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="modal fade" id="modal_edit_parcel_${parcel.id}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pb-0">
                <h4 class="modal-title text-dark mx-auto">
                    <b>Envoi N°${parcel.id}</b>
                </h4>
                <button type="button" class="close text-danger pt-4 ml-0" data-dismiss="modal">
            <span>
              <i class="now-ui-icons ui-1_simple-remove lg"></i>
            </span>
                </button>
            </div>
            <div class="modal-body">
                <c:if test="${canEdit}">
                <form class="form-inline" method="post" action="<c:url  value="/parcel/status/update"/>">
                    <input type="hidden" name="parcel_id" value="${parcel.id}">
                <select class="form-control-sm" name="stat_id">
                    </c:if>
                    <c:if test="${!canEdit}">
                    <select class="form-control-sm" name="stat_id" disabled>
                        </c:if>
                        <c:forEach var="status" items="${statuses}">
                            <c:if test="${status.id == parcel.etat.id}">
                                <option value="${status.id}" selected="selected">${status.name}</option>
                            </c:if>
                            <c:if test="${status.id != parcel.etat.id}">
                                <option value="${status.id}">${status.name}</option>
                            </c:if>
                        </c:forEach>
                    </select>
                        <c:if test="${canEdit}">
                        <input type="submit" class="btn btn-primary" value="modifier" />
                        </form>
                        </c:if>
                    <%-- Ajouter une localisation --%>
                    <c:if test="${canEdit}">
                        <div class="d-flex flex-row mt-3 mb-3">
                            <form method="post" action="<c:url value="/suivis/create" />">
                                <input type="number" name="latitude" class="form-control" placeholder="lat"/>
                                <input type="number" name="longitude" class="form-control ml-1" placeholder="long"/>
                                <input type="text" name="emplacement" class="form-control ml-1" placeholder="emplacement"/>
                                <input type="hidden" name="parcel_id" value="${parcel.id}">
                                <input type="submit" class="btn btn-dark ml-1" value="Ajouter une localisation">
                            </form>
                        </div>
                    </c:if>

                    <%-- Afficher les suivis --%>
                    <c:if test="${parcel.suivis.isEmpty()}">
                    <p> Pour l'instant il n'y a aucun suivis</p>
                    </c:if>
                    <c:if test="${!parcel.suivis.isEmpty()}">
                    <ul class="list-group list-group-flush">
                        <c:forEach var="suivis" items="${parcel.suivis}">
                            <li class="list-group-item">${suivis.emplacement} - lat:${suivis.latitude} ,
                                long:${suivis.longitude}</li>
                        </c:forEach>
                    </ul>
                    </c:if>
            </div>

            <div class="modal-footer d-flex flex-row">
                <%--                <button type="button" class="btn btn-secondary">Nice Button</button>--%>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
