INSERT INTO clients(client_id, client_age, client_name) VALUES (1, 12, "clément Le Bras")
INSERT INTO clients(client_id, client_age, client_name) VALUES (2, 45, "vivi La Lina")
INSERT INTO clients(client_id, client_age, client_name) VALUES (3, 21, "sheppy Le Metallo")

INSERT INTO etats(etat_id, etat_name) VALUES (1, "Enregistrement")
INSERT INTO etats(etat_id, etat_name) VALUES (2, "En attente")
INSERT INTO etats(etat_id, etat_name) VALUES (3, "En acheminement")
INSERT INTO etats(etat_id, etat_name) VALUES (4, "Bloqué")
INSERT INTO etats(etat_id, etat_name) VALUES (5, "Livré")

INSERT INTO colis(colis_id, colis_poids, colis_valeur, colis_origine, colis_destination, etat_id) VALUES (1, 12, 10, "Mayotte", "Sénégale", 2 )
INSERT INTO colis(colis_id, colis_poids, colis_valeur, colis_origine, colis_destination, etat_id) VALUES (2, 9, 15, "Réunion", "Paris", 1 )
INSERT INTO colis(colis_id, colis_poids, colis_valeur, colis_origine, colis_destination, etat_id) VALUES (3, 30, 100, "Paris", "Chambéry", 1 )